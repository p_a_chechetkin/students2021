package stormnet.lesson8;

public class MoreThanFiveNumberException extends WrongNumberException {
	public MoreThanFiveNumberException(Integer number) {
		super("Number was more than five. Number was ", number);
	}
}
