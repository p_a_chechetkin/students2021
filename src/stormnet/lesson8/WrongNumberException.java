package stormnet.lesson8;

public class WrongNumberException extends Exception {
	public WrongNumberException(String errorMessage, Integer number) {
		super(errorMessage + number);
	}
}
