package stormnet.lesson8;

import java.util.Scanner;

public class Part2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = -1;
		while (number != 0) {
			System.out.print("Print number which you want : ");
			try {
				number = sc.nextInt();
				NumberValidator.validate(number);
				System.out.println("Cool number");
			} catch (LessThanZeroNumberException ignore) {
				System.out.println();
			} catch (WrongNumberException e) {
				System.out.println("Common");
				e.printStackTrace();
			} catch (Exception e) {
				System.out.println("Somthing wrong");
				number = 0;
			}
		}
	}
}
