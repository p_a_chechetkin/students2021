package stormnet.lesson8;

import java.util.Scanner;

public class Part1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = -1;
		while (number != 0) {
			System.out.print("Print number which you want : ");
			number = sc.nextInt();
			try {
				NumberValidator.validate(number);
				System.out.println("Cool number");
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("!!RUNTIME!!");
			} finally {
				System.out.println("FOREVER");
			}
			System.out.println("Not forever");
		}
	}
}
