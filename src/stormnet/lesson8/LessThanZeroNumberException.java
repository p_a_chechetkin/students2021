package stormnet.lesson8;

public class LessThanZeroNumberException extends WrongNumberException {
	public LessThanZeroNumberException(Integer number) {
		super("Number was less than zero. Number was ", number);
	}
}
