package stormnet.lesson8;

public class NumberValidator {

	public static void validate(int number) throws WrongNumberException {
		if (number == -100) {
			throw new WrongNumberException("-100??? Seriosly????!!!", number);
		}
		if (number < 0) {
			throw new LessThanZeroNumberException(number);
		} else if (number > 5) {
			throw new MoreThanFiveNumberException(number);
		}
	}
}
