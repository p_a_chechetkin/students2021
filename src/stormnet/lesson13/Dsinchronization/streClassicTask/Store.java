package stormnet.lesson13.Dsinchronization.streClassicTask;

public class Store {
	final int N = 2;
	int counter = 0;

	synchronized int put() {
		if (counter <= N) {
			counter++;
			System.out.println("Sklad contains " + counter + " things");
			return 1;
		}
		return 0;
	}

	synchronized int get() {
		if (counter > 0) {
			counter--;
			System.out.println("Sklad contains " + counter + " things");
			return 1;
		}
		return 0;
	}


}
