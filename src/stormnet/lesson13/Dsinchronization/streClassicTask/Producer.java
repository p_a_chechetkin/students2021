package stormnet.lesson13.Dsinchronization.streClassicTask;

public class Producer extends Thread {
	Store store;
	int product = 5;

	public Producer(Store store) {
		this.store = store;
	}

	public void run() {
		try {
			while (product > 0) {
				product = product - store.put();
				System.out.println("Producer still " + product);
				sleep(100);
			}
		} catch (InterruptedException e) {
			System.out.println("Producer thread interrupted");
		}
	}
}
