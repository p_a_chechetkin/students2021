package stormnet.lesson13.Dsinchronization.streClassicTask;

public class Consumer extends Thread {
	Store store;
	int product = 0;

	public Consumer(Store store) {
		this.store = store;
	}

	public void run() {
		try {
			while (product < 5) {
				product = product + store.get();
				System.out.println("Consumer got products : " + product);
				sleep(1000);
			}
		} catch (InterruptedException e) {
			System.out.println("Consumer thread interrupted");
		}
	}
}
