package stormnet.lesson13.Dsinchronization.synchronByMethod;

public class Main {
	public static void main(String[] args) {
		CommonResource cr = new CommonResource();

		for (int i = 0; i < 3; i++) {
			Thread t = new Thread(new CountThread(cr));
			t.setName("Thread  " + i);
			t.start();
		}
	}
}
