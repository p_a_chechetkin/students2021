package stormnet.lesson13.Dsinchronization.sincromByObject;

public class Automobile implements Runnable {
	Perekrestok perecrestok;
	String autname;

	public Automobile(Perekrestok perecrestok, String autname) {
		this.perecrestok = perecrestok;
		this.autname = autname;
	}

	@Override
	public void run() {
		synchronized (perecrestok) {
			System.out.println("V'exal na perekrestok " + autname);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			System.out.println("UEXAL na perekrestok " + autname);
		}
	}
}
