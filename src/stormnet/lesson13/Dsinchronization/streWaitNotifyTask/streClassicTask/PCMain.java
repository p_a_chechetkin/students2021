package stormnet.lesson13.Dsinchronization.streWaitNotifyTask.streClassicTask;

public class PCMain {
	public static void main(String[] args) {
		Store store = new Store();

		new Thread(new Producer(store)).start();

		new Thread(new Consumer(store)).start();
	}
}
