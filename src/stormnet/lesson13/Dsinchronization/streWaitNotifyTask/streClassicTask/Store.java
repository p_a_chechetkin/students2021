package stormnet.lesson13.Dsinchronization.streWaitNotifyTask.streClassicTask;

public class Store {
	private int product = 0;

	synchronized void put() {
		while (product >= 3) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		product++;
		System.out.println("added || 1 tovar");
		System.out.println("Tovarov = " + product);
		System.out.println();
		notify();
	}

	synchronized void get() {
		while (product < 1) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		product--;
		System.out.println("Kepil 1 tovar");
		System.out.println("Tovarov = " + product);
		System.out.println();
		notify();
	}


}
