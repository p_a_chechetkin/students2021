package stormnet.lesson13.Dsinchronization.streWaitNotifyTask.streClassicTask;

public class Producer extends Thread {
	Store store;

	public Producer(Store store) {
		this.store = store;
	}

	public void run() {
		for (int i = 0; i < 6; i++) {
			store.put();
		}
	}
}
