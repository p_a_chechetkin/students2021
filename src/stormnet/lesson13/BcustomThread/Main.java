package stormnet.lesson13.BcustomThread;

public class Main {
	public static void main(String[] args) {
		System.out.println("Start main");
		MyOwnExtendedThread thread1 = new MyOwnExtendedThread("First");
		Thread thread2 = new Thread(new MyOwnRunnableThread());
		thread1.start();
		thread2.start();

		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}



		System.out.println("End main");
	}
}
