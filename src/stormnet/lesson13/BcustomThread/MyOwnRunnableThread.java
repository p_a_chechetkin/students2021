package stormnet.lesson13.BcustomThread;

public class MyOwnRunnableThread implements Runnable{
	@Override
	public void run() {

		System.out.println("Start work runnable" + Thread.currentThread().getName());
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Stop work runnable" + Thread.currentThread().getName());
	}
}
