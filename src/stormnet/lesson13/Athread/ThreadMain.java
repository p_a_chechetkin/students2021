package stormnet.lesson13.Athread;

public class ThreadMain {
	public static void main(String[] args) {
		Thread thread = Thread.currentThread();
		thread.setName("notMain");
		System.out.println(thread.getName());

		System.out.println();

		extracted(thread);
	}

	private static void extracted(Thread thread) {
		for (StackTraceElement ste : thread.getStackTrace()) {
			System.out.println(ste);
		}
	}
}
