package stormnet.lesson13.HomeworkExample;

public class Main {
	public static void main(String[] args) {
		Table table = new Table();
		Thread player1 = new Thread(new PlayerThread(table));
		Thread player2 = new Thread(new PlayerThread(table));
		Thread player3 = new Thread(new PlayerThread(table));
		Thread lead = new Thread(new LeadThread(table));

		player1.start();
		player2.start();
		player3.start();
		lead.start();
	}
}
