package stormnet.lesson13.HomeworkExample;

import static java.lang.Thread.sleep;

public class PlayerThread implements Runnable {
	private final Table table;
	private int countOfCards = 10;

	public PlayerThread(Table table) {
		this.table = table;
	}

	@Override
	public void run() {
		do {
			try {
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (table) {
				table.putCard(1);
				countOfCards--;
				System.out.println("Player " + Thread.currentThread().getName() + " placed 1 card. Cards " + countOfCards);
			}
		} while (countOfCards > 0);
	}
}
