package stormnet.lesson13.HomeworkExample;

public class LeadThread extends Thread {
	private Table table;

	public LeadThread(Table table) {
		this.table = table;
	}

	@Override
	public void run() {
		int currentCount;
		do {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			currentCount = table.getAllCards();
			if (currentCount == 0) {
				System.out.println("Got cards: " + currentCount);
			} else {
				System.out.println("Have not cards on table");
			}
		} while (currentCount > 0);
	}
}
