package stormnet.lesson13.HomeworkExample;

public class Table {
	private int countOfCards;

	public void putCard(int cards) {
		this.countOfCards += cards;
	}

	public int getAllCards() {
		int counterContainer = this.countOfCards;
		this.countOfCards = 0;
		return counterContainer;
	}
}
