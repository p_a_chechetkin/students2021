package stormnet.lesson13.Cjoin;

public class Main {
	public static void main(String[] args) {
		System.out.println("Start main");
		MyOwnExtendedThread thread1 = new MyOwnExtendedThread("First");

		thread1.start();

		try {
			thread1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("End main");
	}
}
