package stormnet.lesson13.Cjoin;

public class MyOwnExtendedThread extends Thread {
	public MyOwnExtendedThread(String name) {
		super(name);
	}

	@Override
	public void run() {
		System.out.println("Start work extended " + Thread.currentThread().getName());
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Stop work extended" + Thread.currentThread().getName());
	}
}
