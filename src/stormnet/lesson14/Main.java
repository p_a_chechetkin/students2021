package stormnet.lesson14;

interface Checker {
	boolean check(int a);
}

class Dog {
	String name;

	public Dog(String name) {
		this.name = name;
	}

	public static String gav() {
		return "gav";
	}

	public static boolean lech(int x) {
		return x > 0;
	}


}

public class Main {
	public static void main(String[] args) {

		int[] numbers = {0, 1, -5, 12, 15, -7};
//
		printResult(numbers, (x) -> x > 0);
		printResult(numbers, Dog::lech);
		printResult(numbers, Dog::lech);
//
//		Checker checkerForLessThanNought = (number) -> {
//			if (number <= 0) {
//				return true;
//			} else {
//				return false;
//			}
//		};
//
//		printResult(numbers, checkerForLessThanNought);
//		printResult((s) -> {
//			return "I leg" + s;
//		});

//		printDog(Dog::new);

	}

	private static void printResult(int[] numbers, Checker checker) {
		for (int number : numbers) {
			System.out.println("Number " + number + " return " + checker.check(number));
		}


	}
}
