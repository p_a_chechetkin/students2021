package stormnet.lesson14.runnable;

public class Main {
	public static void main(String[] args) {
		Runnable runnable = () -> {
			System.out.println("Hi plot");
			throw new RuntimeException("Oi");
		};
		workWithIt(runnable);
	}

	private static void workWithIt(Runnable runnable) {
		try {
			runnable.run();
		} catch (RuntimeException e) {
			System.out.println("something wrong");
		}
	}
}
