package stormnet.lesson6.anonimClass;

public class Main1 {

	public static void main(String[] args) {
		// Анонимный класс имеет доступ к переменным внешнего класса
		String news = "myNews";

// Анонимные классы
		AnySystem system1 = new AnySystem() {
			@Override
			void getInfo() {
				System.out.println(getNews("Do you know that : " + news));
			}

			// Возможно допиливать методы в анонимном классе
			public String getNews(String text) {
				return "Best news : " + text;
			}
		};

		system1.getInfo();

		// Видим такой класс только внутри текущего метода
	}
}
