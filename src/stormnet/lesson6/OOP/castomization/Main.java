package stormnet.lesson6.OOP.castomization;

import stormnet.lesson6.OOP.abstraction.Auto;
import stormnet.lesson6.OOP.abstraction.BMV;
import stormnet.lesson6.OOP.abstraction.CocaColaBMV;

public class Main {
	public static void main(String[] args) {
		BMV auto;
		Auto auto2 = new CocaColaBMV(123);
		auto = (BMV) auto2;


		// without cast have not method takeColaToAll
//		auto2.takeCollaToAll();
		((CocaColaBMV)auto2).takeCollaToAll();
	}
}
