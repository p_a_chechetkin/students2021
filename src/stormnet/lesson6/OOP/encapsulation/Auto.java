package stormnet.lesson6.OOP.encapsulation;

public class Auto {
	private final String colour;
	private int countOfWheels;

	public Auto(int countOfWheel, String colour) {
		this.countOfWheels = countOfWheel;
		this.colour = colour;
	}

	public boolean startEngine() {
		// many actions with key, gas, and engine under a hood
		return countOfWheels == 4;
	}

	public String getColour() {
		return colour;
	}

	public void addWheel() {
		// difficult logic with adding wheel to auto
		countOfWheels++;
	}

	public void removeWheel() {
		// difficult logic with adding wheel to auto
		countOfWheels--;
	}

}
