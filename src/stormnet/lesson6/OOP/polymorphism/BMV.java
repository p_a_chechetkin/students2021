package stormnet.lesson6.OOP.polymorphism;

public class BMV extends Auto {
	public BMV(int countOfWheels, int seats, String color) {
		super(countOfWheels, seats, color);
	}

	@Override
	public void run() {
		System.out.println("Brrrrr");
	}
}
