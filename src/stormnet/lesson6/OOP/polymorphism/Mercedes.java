package stormnet.lesson6.OOP.polymorphism;

public class Mercedes extends Auto {
	public Mercedes(int countOfWheels, int seats, String color) {
		super(countOfWheels, seats, color);
	}

	@Override
	public void run() {
		System.out.println("Vrum Vrum");
	}
}
