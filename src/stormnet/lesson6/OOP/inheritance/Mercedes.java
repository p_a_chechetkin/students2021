package stormnet.lesson6.OOP.inheritance;

public class Mercedes extends Auto {
	public Mercedes(int countOfWheels, int seats, String color) {
		super(countOfWheels, seats, color);
	}

	public void turnOnLight() {
	}
}
