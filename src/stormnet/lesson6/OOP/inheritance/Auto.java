package stormnet.lesson6.OOP.inheritance;

public class Auto {
	protected int countOfWheels;
	protected int seats;
	protected String color;

	public Auto(int countOfWheels, int seats, String color) {
		this.countOfWheels = countOfWheels;
		this.seats = seats;
		this.color = color;
	}

	public void run() {
		System.out.println("Чих пых .. куда ехать.. кто я ?");
	}

}
