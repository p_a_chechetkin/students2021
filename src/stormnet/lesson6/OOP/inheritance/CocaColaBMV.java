package stormnet.lesson6.OOP.inheritance;

public final class CocaColaBMV extends BMV {
	private final int countOfCola;

	public CocaColaBMV(int countOfColas) {
		super(4, 2, "RED");
		this.countOfCola = countOfColas;
	}

	public int getCountOfCola() {
		return countOfCola;
	}

	public void getToAllButtleOfColla() {

	}

}
