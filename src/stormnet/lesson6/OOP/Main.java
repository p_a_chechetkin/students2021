package stormnet.lesson6.OOP;

import stormnet.lesson6.OOP.polymorphism.*;

public class Main {
	public static void main(String[] args) {

		// polymorphism
		Auto auto = new Auto(20, 0, "Brown");
		auto.run();

		BMV bmv = new BMV(5, 4, "Gray");
		bmv.run();

		Mercedes mercedes = new Mercedes(4, 2, "Green");
		mercedes.run();

		CocaColaBMV colaCar = new CocaColaBMV(125);
		colaCar.run();



		// INHERITANCE
//		Auto auto = new Auto(20, 0, "Brown");
//		auto.run();
//
//		BMV bmv = new BMV(5, 4, "Gray");
//		bmv.doCoolRight();
//		bmv.run();
//
//		Mercedes mercedes = new Mercedes(4, 2, "Green");
//		mercedes.turnOnLight();
//		mercedes.run();
//
//		CocaColaBMV colaCar = new CocaColaBMV(125);
//		colaCar.run();
//		colaCar.doCoolRight();
//		System.out.println(colaCar.getCountOfCola());
//		colaCar.getToAllButtleOfColla();
//		colaCar.countOfWheels = 100;
	}
}
