package stormnet.lesson6.OOP.abstraction;

public class CocaColaBMV extends BMV implements Garageble{
	private int countOfCola;

	public CocaColaBMV(int countOfColas) {
		super(4, 2, "RED");
	}

	public void takeCollaToAll(){}

	public void setColor(String color){
		this.color = color;
	}

	@Override
	public void takeIntoGarage() {

	}

	@Override
	public String moveOnPogruchik() {
		return Garageble.super.moveOnPogruchik();
	}
}
