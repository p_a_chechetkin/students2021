package stormnet.lesson6.OOP.abstraction;

public interface Garageble {

	int ALL_VARIABLE = 4;

	static String cert() {
		return "Сертификат";
	}

	void takeIntoGarage();

	default String moveOnPogruchik() {
		setValue();
		return "Поднята";
	}

	private void setValue(){

	}
}
