package stormnet.lesson6.OOP.abstraction;

public class Mercedes extends Auto implements Garageble {
	public Mercedes(int countOfWheels, int seats, String color) {
		super(countOfWheels, seats, color);
	}

	@Override
	public void run() {
		System.out.println("Vrum Vrum");
	}

	@Override
	public void takeIntoGarage() {

	}

	@Override
	public String moveOnPogruchik() {
		return Garageble.super.moveOnPogruchik();
	}
}
