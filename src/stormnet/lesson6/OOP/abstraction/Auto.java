package stormnet.lesson6.OOP.abstraction;

public abstract class Auto {
	protected int countOfWheels;
	protected int seats;
	protected String color;

	public Auto(int countOfWheels, int seats, String color) {
		this.countOfWheels = countOfWheels;
		this.seats = seats;
		this.color = color;
	}

	public String getColourOfCar(){
		return "Current color" + color;
	}


	public abstract void run();

	public int doRight(){
		System.out.println("done");
		return 1;
	}
}
