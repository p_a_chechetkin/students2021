package stormnet.lesson16.optional;

import java.util.Optional;

public class R_OptionalExample {
	public static void main(String[] args) {
		Optional<String> optNullString = Optional.ofNullable(null);
		Optional<String> optNotNullString = Optional.of("ExistingString");

//		System.out.println(optNullString);
//		System.out.println(optNotNullString.get());

//		System.out.println("________ isPresent ___________");
//
//		System.out.println(optNullString.isPresent());
//		System.out.println(optNotNullString.isPresent());

//		System.out.println("________ orElse ___________");
//
//		System.out.println(optNullString.orElse("String was null"));
//		System.out.println(optNotNullString.orElse("I cannot place here not String value"));
//
//		System.out.println("________ orElseGET ___________");
//
//		int a = 1;
//		System.out.println(optNullString.orElseGet(() -> {
//			String s = " Hi " + "Hello";
//			if (a == 1) {
//				return s;
//			}
//			return "Value was null";
//		}));
//		System.out.println(optNotNullString.orElseGet(() -> {
//			return "Value not null =) ";
//		}));

//
//		System.out.println("________ orElseThrow ___________");
//
//		optNullString.orElseThrow(RuntimeException::new); // Supplier<String>
//		optNotNullString.orElseThrow(() -> new AssertionError("Message")); // Supplier<String>

		System.out.println("________ ifPresent ___________");

		optNullString.ifPresent(System.out::println); // Consumer<String>
		optNullString.ifPresent(s -> System.out.println(s.length())); // Consumer<String>
		optNotNullString.ifPresent(s -> System.out.println("W" + s.length())); // Consumer<String>


		System.out.println("________ ifPresentOrElse ___________");

		optNullString.ifPresentOrElse(
				v -> System.out.println(v), //ifPresent
				// orElse
				() -> {
					throw new AssertionError("Message");
				}
		);
		optNotNullString.ifPresentOrElse(
				v -> System.out.println(v),        // Consumer , Runnable
				() -> System.out.println("Value was null"));
//
//
	}
}
