package stormnet.lesson16.streams.terminalOperations;

import java.util.ArrayList;
import java.util.Collections;

public class P_ReduceExample {
	public static void main(String[] args) {
		ArrayList<Integer> numbers = new ArrayList<>();
		Collections.addAll(numbers, -1, 1, 0, 3, 4, 0);

		System.out.println(
				numbers.stream().reduce((x, y) -> {
			return x + y;
		}).get()
		);

//		identity - default value
		numbers.stream().reduce(0, (x, y) -> {
			return x + y;
		});// BinaryOperator<T>
	}
}
