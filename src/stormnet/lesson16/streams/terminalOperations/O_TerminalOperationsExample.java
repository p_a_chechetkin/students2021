package stormnet.lesson16.streams.terminalOperations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

public class O_TerminalOperationsExample {
	public static void main(String[] args) {
		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 700),
				new Phone("Samsung", 1000));

//		long count = phones.stream()
//				.filter(p -> p.getName().length() > 6) // Predicate<Phone>
//				.count();
//		System.out.println(count);

		Optional<Phone> phone = phones.stream()
				.filter(p -> p.getName().length() > 6) // Predicate<Phone>
				.findFirst();
		phone.ifPresent(p -> {
			String result = p.getName();
			System.out.println(result + p.getPrice());
		});

//		Phone phone = phones.stream()
//				.filter(p -> p.getName().length() > 6) // Predicate<Phone>
//				.findAny()
//				.get();
//		System.out.println(phone);

//		boolean isAllMatch = phones.stream()
//				.allMatch(p -> p.getName().length() > 6); // Predicate<Phone>
//		System.out.println(isAllMatch);
//
//		boolean isAnyMatch = phones.stream()
//				.anyMatch(p -> p.getName().length() > 6); // Predicate<Phone>
//		System.out.println(isAnyMatch);
//
//		boolean isNoneMatch = phones.stream()
//				.noneMatch(p -> p.getName().length() > 6); // Predicate<Phone>
//		System.out.println(isNoneMatch);
//
//		Phone phone = phones.stream()
//				.min((p1, p2) -> p1.getName().length() - p2.getName().length())
//				.get(); // Comparator<Phone>
//		System.out.println(phone);
////
//		Phone phone2 = phones.stream()
//				.max((p1, p2) -> p1.getName().length() - p2.getName().length())
//				.get(); // Comparator<Phone>
//		System.out.println(phone2);
	}
}
