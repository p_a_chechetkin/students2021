package stormnet.lesson16.streams.terminalOperations;

import stormnet.lesson16.phone.Phone;

import java.util.*;
import java.util.stream.Collectors;

public class S_CollectExample {
	public static void main(String[] args) {
		ArrayList<String> cities = new ArrayList<>();
		Collections.addAll(cities, "Paris", "London", "Madrid");

		List<String> filteredCollectionList = cities.stream()
				.filter(c -> c.length() > 6)
				.collect(Collectors.toList());

		Set<String> filteredCollectionSet = cities.stream()
				.filter(c -> c.length() > 6)
				.collect(Collectors.toSet());


		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 700),
				new Phone("Samsung", 1000));

		Map<String, Integer> filteredCollectionMap = phones.stream()
				.collect(Collectors.toMap(p -> p.getName(), p -> p.getPrice()));


	}
}
