package stormnet.lesson16.streams.terminalOperations;

import java.util.ArrayList;
import java.util.Collections;

public class F_ForeachExample {
	public static void main(String[] args) {
		ArrayList<String> cities = new ArrayList<>();
		Collections.addAll(cities, "Gomel", "Minsk", "Vorclav");

		cities.stream()
				.forEach(s -> System.out.println(s));        // Consumer
	}
}
