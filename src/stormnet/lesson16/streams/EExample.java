package stormnet.lesson16.streams;

import java.util.stream.Stream;

public class EExample {
	public static void main(String[] args) {
		Stream<String> citiesStream = Stream.of("Paris", "London", "Madrid");
		citiesStream.forEach(c -> System.out.println(c));
	}
}
