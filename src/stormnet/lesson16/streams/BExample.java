package stormnet.lesson16.streams;

import java.util.ArrayList;
import java.util.Collections;

public class BExample {
	public static void main(String[] args) {
		ArrayList<Integer> numbers = new ArrayList<>();
		Collections.addAll(numbers, -1, 1, 0, 2, -7, 4, 0);

		System.out.println(numbers.stream().filter(number -> number > 0).count());
	}

}
