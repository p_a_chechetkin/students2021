package stormnet.lesson16.streams;

import java.util.ArrayList;
import java.util.Collections;

public class AExample {
	public static void main(String[] args) {
		ArrayList<Integer> numbers = new ArrayList<>();
		Collections.addAll(numbers, -1, 1, 0, 2, -7, 4, 0);

		int countOfNumbersGreaterThanZero = 0;
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.get(i) > 0) {
				countOfNumbersGreaterThanZero++;
			}
		}

		System.out.println(countOfNumbersGreaterThanZero);
	}
}
