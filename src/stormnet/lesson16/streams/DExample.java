package stormnet.lesson16.streams;

import java.util.Arrays;
import java.util.stream.Stream;

public class DExample {
	public static void main(String[] args) {
		Stream<String> citiesStream = Arrays.stream(new String[]{"Paris", "London", "Madrid"});
		citiesStream.forEach(c -> System.out.println(c));
	}
}
