package stormnet.lesson16.streams;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class CExample {
	public static void main(String[] args) {
		ArrayList<String> cities = new ArrayList<>();
		Collections.addAll(cities, "Paris", "London", "Madrid");

		cities.stream().filter(s -> s.length() == 6).forEach(System.out::println);

		Stream<String> citiesStream = cities.stream();
		citiesStream = citiesStream.filter(s -> s.length() == 6);
		Phone phone = new Phone("", 1);
		citiesStream.forEach(elIzStream -> phone.setName(elIzStream));
		citiesStream.forEach(phone::setName);
//
//
		System.out.println(citiesStream.count());
	}
}
