package stormnet.lesson16.streams.operations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class J_FlatMappingExample {
	public static void main(String[] args) {

		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 700),
				new Phone("Samsung", 1000));

		phones.stream()
				.flatMap(p -> {
					return Stream.of(
							"Name : " + p.getName() + " price: " + p.getPrice(),
							"Name: " + p.getName() + " Sale price: " + (p.getPrice() - 100),
							"New phone cool"
					);
				})
				.forEach(System.out::println);
	}
}
