package stormnet.lesson16.streams.operations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;

public class H_PhoneExample {
	public static void main(String[] args) {
		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 700),
				new Phone("Samsung", 1000));

		phones.stream()
				.filter(p -> p.getPrice() < 800)
				.forEach(p -> System.out.println(p.getName()));
	}
}
