package stormnet.lesson16.streams.operations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;

public class I_MappingExample {
	public static void main(String[] args) {

		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 700),
				new Phone("Samsung", 1000));

		phones.stream()
				.map(p -> "Name : " + p.getName() + " price: " + p.getPrice()) // Function<Phone, String>

				.forEach(System.out::println);
	}
}
