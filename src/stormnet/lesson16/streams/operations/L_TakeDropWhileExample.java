package stormnet.lesson16.streams.operations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;

public class L_TakeDropWhileExample {
	public static void main(String[] args) {
		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),				new Phone("Xiaomi", 700),				new Phone("Samsung", 1000));

		phones.stream()
				.takeWhile(p -> {
					return p.getPrice() < 0;
				}) // Predicate<Phone>
				.forEach(System.out::println);

		System.out.println("________________________________________________________");

		phones.stream()
				.dropWhile(p -> {
					return p.getPrice() < 0;
				}) // Predicate<Phone>
				.forEach(System.out::println);
	}
}
