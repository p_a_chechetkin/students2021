package stormnet.lesson16.streams.operations;

import java.util.ArrayList;
import java.util.Collections;

public class G_FilterExample {
	public static void main(String[] args) {
		ArrayList<String> cities = new ArrayList<String>();
		Collections.addAll(cities, "Gomel", "Minsk", "Vorclav");

		cities.stream()
				.filter(elementOfStream -> elementOfStream.length() < 6) // Predicate
				.forEach(System.out::println); // Consumer
	}
}
