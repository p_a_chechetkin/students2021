package stormnet.lesson16.streams.operations;

import java.util.stream.Stream;

public class N_SkipLimitExample {
	public static void main(String[] args) {
		Stream<String> people = Stream.of("Tom", "Bob", "Sam", "Bob", "Kate", "Sam");
		people.skip(2).forEach(System.out::println);

		System.out.println("______________________________________________________________");

		people = Stream.of("Tom", "Bob", "Sam", "Bob", "Kate", "Sam");

		people.limit(2).forEach(System.out::println);
	}
}
