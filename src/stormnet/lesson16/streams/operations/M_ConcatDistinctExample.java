package stormnet.lesson16.streams.operations;

import java.util.stream.Stream;

public class M_ConcatDistinctExample {
	public static void main(String[] args) {
		Stream<String> people1 = Stream.of("Tom", "Bob", "Sam");
		Stream<String> people2 = Stream.of("Alice", "Kate", "Sam");

		Stream.concat(people1, people2).forEach(System.out::println);

		System.out.println("________________________________________________________");

		Stream<String> people = Stream.of("Tom", "Bob", "Sam", "Bob", "Kate", "Sam");
		people.distinct().forEach(System.out::println);
	}
}
