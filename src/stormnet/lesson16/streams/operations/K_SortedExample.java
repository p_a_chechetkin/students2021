package stormnet.lesson16.streams.operations;

import stormnet.lesson16.phone.Phone;

import java.util.ArrayList;
import java.util.Collections;

public class K_SortedExample {
//	public static void main(String[] args) {
//		ArrayList<String> cities = new ArrayList<>();
//		Collections.addAll(cities, "Gomel", "Minsk", "Vorclav", "Amsterdam");
//
//		cities.stream()
//				.sorted()
//				.forEach(System.out::println);
//
//	}

	public static void main(String[] args) {
		ArrayList<Phone> phones = new ArrayList<>();
		Collections.addAll(phones,
				new Phone("Siemens", -100),
				new Phone("Xiaomi", 300),
				new Phone("Xiaomi", 700),
				new Phone("Xiaomi", 600),
				new Phone("Samsung", 1000));

		phones.stream()
				.sorted((p1, p2) -> {
					int compareByName = p1.getName().length() - p2.getName().length();
					if (compareByName == 0) {
						return p2.getPrice() - p1.getPrice();
					}
					return compareByName;
				})
				.forEach(System.out::println);
	}
}
