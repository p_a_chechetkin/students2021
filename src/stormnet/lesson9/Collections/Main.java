package stormnet.lesson9.Collections;

import java.util.ArrayList;
import java.util.Comparator;

public class Main {
	public static void main(String[] args) {
		ArrayList<Dog> list = new ArrayList<>();
		list.add(new Dog("qwe", 4));
		list.add(new Dog("qwe", 3));
		list.add(new Dog("aaa", 3));
		CollectionsMain.printCollection(list);

		Comparator<Dog> comparator = new DogByAgeComparator()
				.thenComparing(new DogByNameComparator());

		list.sort(comparator);

		CollectionsMain.printCollection(list);


	}
}
