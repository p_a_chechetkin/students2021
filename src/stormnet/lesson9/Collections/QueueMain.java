package stormnet.lesson9.Collections;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class QueueMain {
	public static void main(String[] args) {
		Queue<String> queue = new LinkedList<>();
		try {
			System.out.println(queue.element()); // NoSuchElementException if empty
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		System.out.println(queue.offer("123"));
		System.out.println(queue.element());
		printQueue(queue);

		System.out.println(queue.offer("qwe"));
		printQueue(queue);

		System.out.println(queue.peek()); // null if empty | Without remove
		printQueue(queue);

		System.out.println(queue.remove()); // NoSuchElementException if empty | And remove
		printQueue(queue);

		System.out.println(queue.poll()); // null if empty | And remove
		printQueue(queue);


	}

	private static void printQueue(Queue<String> queue) {
		System.out.println("Queue");
		String[] strings = new String[queue.size()];
		for (String s : queue.toArray(strings)) {
			System.out.println(s);
		}
	}
}
