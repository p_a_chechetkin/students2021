package stormnet.lesson9.Collections;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import static java.util.Comparator.nullsFirst;
import static stormnet.lesson9.Collections.CollectionsMain.printCollection;

public class ListMain {
	public static void main(String[] args) {
		List<String> firstList = new ArrayList<>();
		firstList.add("123");
		firstList.add("asd");
		firstList.add("qwe");
		firstList.add("asd");
		System.out.println(firstList.get(0));
		System.out.println(firstList.get(2));

		System.out.println(firstList.contains("asd"));
		System.out.println(firstList.lastIndexOf("asd"));

//		ListIterator<String> listIterator = firstList.listIterator();
//		if (listIterator.hasNext()) {
//			System.out.println(listIterator.previousIndex());
//			System.out.println(listIterator.next());
//		}
//
//		ArrayList<String> secondList = (ArrayList<String>) List.of("123", "14", "asd");
////
//		printCollection(firstList);
//		firstList.set(1, "New element");
//		printCollection(firstList);
////
////		firstList.sort(nullsFirst((o1, o2) -> o2.length() - o1.length()));
////		printCollection(firstList);
////
//		List<String> list = firstList.subList(1, 3);
//		printCollection(list);
//
//

	}
}
