package stormnet.lesson9.Collections;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class DequeMain {
	public static void main(String[] args) {
//		Deque<String> deque = new LinkedList<>();
////		try {
////			System.out.println(deque.element()); // NoSuchElementException if empty
////		} catch (NoSuchElementException e) {
////			e.printStackTrace();
////		}
////
////		System.out.println(deque.offer("123"));
////		System.out.println(deque.element());
////		printQueue(deque);
////
////		System.out.println(deque.offer("qwe"));
////		printQueue(deque);
////
////		System.out.println(deque.peek()); // null if empty | Without remove
////		printQueue(deque);
////
////		System.out.println(deque.remove()); // NoSuchElementException if empty | And remove
////		printQueue(deque);
////
////		System.out.println(deque.poll()); // null if empty | And remove
////		printQueue(deque);
//
//		deque.addFirst("123");
//		printQueue(deque);
//
//		deque.addLast("qwe");
//		printQueue(deque);
//
//		deque.getFirst(); // without removing | NoSuchElementException
//		printQueue(deque);
//
//		deque.getLast(); // without removing | NoSuchElementException
//		printQueue(deque);
//
//		deque.offerFirst("Offer|123");
//		printQueue(deque);
//
//		deque.offerLast("Offer|qwe");
//		printQueue(deque);
//
//		System.out.println(deque.peekFirst()); // witout remove | null
//		printQueue(deque);
//
//		System.out.println(deque.peekLast()); // witout remove | null
//		printQueue(deque);
//
//		System.out.println(deque.pollFirst()); // remove | null
//		printQueue(deque);
//
//		System.out.println(deque.pollLast()); // remove | null
//		printQueue(deque);
//
//		System.out.println(deque.pop()); // first with remove | NoSuchElementException
//		printQueue(deque);
//
//		deque.push("141"); // add before first
//		printQueue(deque);
//
//		System.out.println(deque.removeFirst()); // with remove | NoSuchElementException
//		printQueue(deque);
//
//		System.out.println(deque.removeLast()); // with remove | NoSuchElementException
//		printQueue(deque);
//
//		System.out.println(deque.removeFirstOccurrence("qwe")); // with remove first faced element
//		printQueue(deque);
//
//		System.out.println(deque.removeLastOccurrence("zxc")); // with remove first faced element
//		printQueue(deque);
		Set<String> set = new HashSet<>();
		System.out.println(set.add("123"));
		System.out.println(set.add("555"));
		System.out.println(set.add("555"));
		System.out.println(set.add("123"));
//		CollectionsMain.printCollection(set);

	}

	private static void printQueue(Queue<String> deque) {
		System.out.println("Queue");
		String[] strings = new String[deque.size()];
		for (String s : deque.toArray(strings)) {
			System.out.println(s);
		}
	}
}
