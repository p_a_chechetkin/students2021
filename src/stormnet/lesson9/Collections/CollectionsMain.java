package stormnet.lesson9.Collections;

import java.util.*;

public class CollectionsMain {
	public static void main(String[] args) {
		Collection<String> firstList = new ArrayList<>();
		Collection<String> secondList = new ArrayList<>();

//		firstList.add("123");
//		printCollection(firstList);
//
//		secondList.add("qwe");
//		secondList.add("asd");
//		firstList.addAll(secondList);
//		printCollection(firstList);
//
//		firstList.clear();
//		printCollection(firstList);
////
//		firstList.add("123");
//		printCollection(firstList);
//		System.out.println(firstList.contains("123"));
//		System.out.println(firstList.contains("qwe"));
////
//		printCollection(firstList);
//		System.out.println(firstList.isEmpty());
//		firstList.clear();
//		printCollection(firstList);
//		System.out.println(firstList.isEmpty());
//
//		firstList.add("123");
//		firstList.add("qwe");
//		printCollection(firstList);
//		firstList.remove("123");
//		printCollection(firstList);
////
//		firstList.clear();
//		firstList.add("123");
//		firstList.add("asd");
//		firstList.add("qwe");
//		Iterator<String> iterator = firstList.iterator();
//		if (iterator.hasNext()){
//			System.out.println(iterator.next());
//		}
//
//		firstList.clear();
//		secondList.clear();
//		firstList.add("123");
//		firstList.add("qwe");
//		secondList.add("zxc");
////		secondList.add("123");
////		System.out.println(firstList.removeAll(secondList));
////		printCollection(firstList);
//
////		firstList.clear();
////		secondList.clear();
//		firstList.add("123");
//		firstList.add("qwe");
//		secondList.add("123");
//		secondList.add("zxc");
//		System.out.println(firstList.retainAll(secondList));
//		printCollection(firstList);
////
//		firstList.clear();
//		firstList.add("123");
//		System.out.println(firstList.size());
//		firstList.add("qwe");
//		System.out.println(firstList.size());
////
//		printCollection(firstList);
//		String[] stringArray = new String[firstList.size()];
//		firstList.toArray(stringArray);
//		System.out.println(Arrays.toString(stringArray));

	}

	public static void printCollection(Collection<Dog> firstList) {
		System.out.println("Your list");
		int index = 0;
		for (Dog s : firstList) {
			System.out.println(index++ + " | " + s);
		}
	}
}
