package stormnet.lesson12.inOutStreams;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class InOutStreams {
	public static void main(String[] args) {
		String filePath = "src/resources/MyNewTxt.txt";

		// region File writer
		try (FileWriter fw = new FileWriter(filePath)){
			fw.write("Tasjdkfalkjsdf :)");
			fw.append("text My Own text\n !cool!");
			fw.flush();// clear stream
		} catch (IOException e) {
			e.printStackTrace();
		}

		//region Append=true
//		try (FileWriter fw = new FileWriter(filePath, true)){
//			fw.append("\ntext My Own text\n !cool!");
//			fw.write("THis added by write :)");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		endregion


		//BufferedWriter
//		writeTextIntoFile("", filePath);

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath, true))) {
			bw.write("New text");
			bw.newLine();
			bw.write("2New text2");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// endregion

		// region File Reader
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			String line = br.readLine();
			while (line != null) {
				System.out.println(Arrays.toString(line.split(" ")));
				line = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// endregion

		// region java.NIO
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(filePath));

			lines.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Files.write(Paths.get("src", "resources", "newMyNewTxt.txt"), lines);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// endregion
	}

	private static void writeTextIntoFile(String text, String filePath) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath, true))) {
			bw.write(text);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
