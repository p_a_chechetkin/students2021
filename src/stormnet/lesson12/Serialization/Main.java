package stormnet.lesson12.Serialization;

import java.io.*;

public class Main {
	public static void main(String[] args) {
		Dog dog = new Dog("Cerber");
		Dog dog1 = new Dog("Mercury");

		DogHandler dogHandler = new DogHandler();

		dogHandler.addDog(dog);
		dogHandler.addDog(dog1);


		DogHandler handler = null;
//		region Deserialization
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("dogHandler.obraz"))) {
			handler = (DogHandler) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(handler.getNumber());



//
////		// region Serialization
//		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("dog.obraz"))) {
//			oos.writeObject(dog);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
////
//		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("dog1.obraz"))) {
//			oos.writeObject(dog1);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		// endregion
//
//		Dog cerber = null;
////		region Deserialization
//		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("dog1.obraz"))) {
//			cerber = (Dog) ois.readObject();
//		} catch (IOException | ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		System.out.println(cerber.getName());
//		region Deserialization
//		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("dog.obraz"))) {
//			cerber = (Dog) ois.readObject();
//		} catch (IOException | ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		System.out.println(cerber.getName());
//		endregion









	}
}
