package stormnet.lesson12.Serialization;

import java.io.Serializable;
import java.util.ArrayList;

public class DogHandler implements Serializable {
	private final ArrayList<Dog> dogs = new ArrayList<>();
	private int number = 0;

	public int getNumber() {
		return number;
	}

	public void addDog(Dog dog) {
		this.dogs.add(dog);
	}
}
