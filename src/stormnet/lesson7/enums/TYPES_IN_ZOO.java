package stormnet.lesson7.enums;

import stormnet.lesson7.interfaces.Animal;
import stormnet.lesson7.interfaces.Cat;
import stormnet.lesson7.interfaces.Fish;

public enum TYPES_IN_ZOO {
	FISH,
	CAT;

}
