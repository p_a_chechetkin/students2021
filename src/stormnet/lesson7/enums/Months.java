package stormnet.lesson7.enums;

public enum Months {
	JANUARY(1),
	FEBRUARY(2),
	MARCH(3),
	APRIL(4),
	MAY(5),
	JUNE(6),
	JULY(7),
	AUGUST(8),
	SEPTEMBER(9),
	OCTOBER(10),
	NOVEMBER(11),
	DECEMBER(12);

	private final int monthNumber;

	Months(int monthNumber) {
		this.monthNumber = monthNumber;
	}

	public static String getMonthNameByNamber(Integer monthNumber) {
		for (int i = 0; i < Months.values().length; i++) {
			if (Months.values()[i].getMonthNumber() == monthNumber) {
				return Months.values()[i].name();
			}
		}
		return "Not a month's number";
	}

	public int getMonthNumber() {
		return monthNumber;
	}

	@Override
	public String toString() {
		return "Months{" +
				"monthNumber=" + monthNumber +
				"monthName=" + name() +
				'}';
	}
}
