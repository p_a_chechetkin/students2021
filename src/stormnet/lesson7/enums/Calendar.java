package stormnet.lesson7.enums;

public class Calendar {

	private Months currentMonth;

	public void setCurrentMonth(Months currentMonth) {
		this.currentMonth = currentMonth;
	}

	public Calendar(Months currentMonth) {
		this.currentMonth = currentMonth;
	}

	public Months getCurrentMonth() {
		return currentMonth;
	}

	@Override
	public String toString() {
		return "Calendar{" +
				"currentMonth=" + currentMonth +
				'}';
	}
}
