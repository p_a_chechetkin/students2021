package stormnet.lesson7.interfaces;

public interface Voicible {
	void voice();
}
