package stormnet.lesson7.interfaces;

public class Bird extends Animal implements Voicible, Flyable {

	@Override
	public void voice() {
		System.out.println("Tiu-Tiu");
	}

	@Override
	public void fly() {
		System.out.println("Fly birds ))");
	}

	@Override
	public void pol() {

	}
}
