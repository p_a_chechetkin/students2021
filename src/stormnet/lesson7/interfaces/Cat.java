package stormnet.lesson7.interfaces;

public class Cat extends Animal implements Voicible{
	@Override
	public void voice() {
		System.out.println("Meow");
	}

	@Override
	public void pol() {

	}

	public void pisyt(String s){
		System.out.println(s);
	}
}
